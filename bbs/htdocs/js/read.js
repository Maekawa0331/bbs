/**
 *
 */

$(function() {
	getThread()
});
function getThread(){
    var paths = location.pathname.split("/", 5);
    var thread = document.getElementById("thread");
    var rootUrl = location.protocol + "//" + location.host;


    $.ajax({
        url: rootUrl + "/bbs/getThread/" + paths[3] + (paths[4] ? "/"+paths[4] : ""),
        dataType: "json"
    })
    .done(function(data, textStatus, jqXHR ){
    	//スレッド内容初期化
    	$("#thread").empty();

    	//スレッド内容更新
    	$.each(data["reses"], function(index, res) { $("#thread").append("    <dt><span>" + escapeHtml(res["res_no"]) +  "</span> 名前：<font color=green><b>" + escapeHtml(res["user_name"]) + "</b></font> ：" + escapeHtml(res["createdy"]) +  " ID:" + escapeHtml(res["user_id"]));
    		$("#thread").append("    <dd>" + escapeHtml(res["res_text"]) +  "<br><br>");
    	});
    	//ページングリンク算出
    	var threadUrl = "/bbs/thread/" + paths[3];

       	$("#top").attr("href",rootUrl + "/bbs/top");
       	$("#all").attr("href",rootUrl + threadUrl);

       	//前100
        var n = parseInt($("#thread dt:first span:first").text()) - 1;
        if (!n && $("#thread").children().length > 2) {
        	var m = parseInt($("#thread dt:eq(1) span:first").text());
            if (m > 2){
                n = m - 1;
        	}
        }
        wUrl = n ? rootUrl + threadUrl + "/" + ((n - 99 > 1 ? n - 99 : 1) + "-" + n) : "javascript:void 0;";
       	$("#f100").attr("href",wUrl);

       	//後100
        n = parseInt($("#thread dt:last span:first").text()) + 1;
        wUrl = n <= 1001 ? rootUrl + threadUrl + "/" + (n + "-" + (n + 99)) : "javascript:void 0;";
       	$("#b100").attr("href",wUrl);
       	$("#l50").attr("href",rootUrl + threadUrl + "/l50");
       	$("#reload").off("click.thread");
       	$("#reload").on("click.thread", function() {getThread();});
       	$("#postForm").attr("action",rootUrl + "/bbs/resWrite");
    })
    .fail(function(jqXHR, textStatus, errorThrown){
    	alert(textStatus + ":" + errorThrown);
    });
}
function escapeHtml(content) {
	  return content
	  				.replace(/&/g, '&amp;')
	  				.replace(/</g, '&lt;')
	  				.replace(/>/g, '&gt;')
	  				.replace(/"/g, '&quot;')
	  				.replace(/'/g, '&#39;');
}
