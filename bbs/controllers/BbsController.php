<?php

require_once "ControllerBase.php";
require_once "BbsModel.php";

class BbsController extends ControllerBase
{

    /*Controllerではデータの取得・ビューへの割り当てのみ行う*/

    // TOP画面内容表示処理
    public function indexAction()
    {

        // TOP画面内容情報取得
        $bbs = new BbsModel();
        $bbsTopInfo = $bbs->getBbsTopInfo();

        $this->view->assign('bbsTopInfo', $bbsTopInfo);

    }

    // スレッド一覧表示処理
    public function threadListAction()
    {
        $threadDAO = new ThreadDAO();
        $threadList = $threadDAO->getThreadList();

        $this->view->assign('threadList', $threadList);
    }

    // スレッド詳細表示処理
    public function threadAction()
    {
        $query = $this->request->getParam();
        $bbs = new BbsModel();
        $bbsThreadSubInfo = $bbs->getBbsThreadSubInfo($query);
        $this->view->assign('bbsThreadSubInfo', $bbsThreadSubInfo);
    }

    //スレッド詳細メインデータ取得処理(非同期通信用)
    public function getThreadAction()
    {
        $query = $this->request->getParam();
        $bbs = new BbsModel();
        $bbsThreadMainInfo = $bbs->getBbsThreadMainInfo($query);

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($bbsThreadMainInfo);
        exit();
    }

    public function resWriteAction()
    {
        // レス投稿処理
        $query = $this->request->getPost();
        $bbs = new BbsModel();
        $bbs->addResponse($query);

        // 投稿処理後、スレッド内容表示画面へリダイレクト
        //TODO IPはiniファイルから取得するようにする
        header('Location: http://192.168.100.100/bbs/thread/'. $query['thre_id']. '/l50');
    }

    // スレッド新規作成画面表示処理
    public function threadNewAction()
    {
        $threadNewInfo['session_id'] = sha1(session_id());

        $this->view->assign('threadNewInfo', $threadNewInfo);
    }

    // スレッド作成処理
    public function threadCreateAction()
    {
        $query = $this->request->getPost();
        $bbs = new BbsModel();
        $bbs->addThread($query);

        // 作成処理後、スレッド内容表示画面へリダイレクト
        header('Location: http://192.168.100.100/bbs/threadComp');
    }

    // スレッド作成完了画面表示処理
   public function threadCompAction()
    {
        // スレッド作成時に登録した内容をセッションから取得し、表示
        if (empty($_SESSION['thre_id']) && empty($_SESSION['thre_name'])){
            errorResponse('ページ遷移が不正です。');
        }
        $threadCompInfo['thre_id'] = $_SESSION['thre_id'];
        $threadCompInfo['thre_name'] = $_SESSION['thre_name'];

        $this->view->assign('threadCompInfo', $threadCompInfo);
    }
}
?>
